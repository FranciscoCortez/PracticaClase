<html>
	<head>
		<title>Ejemplo de operadores de Comparacion</title>
	</head>
	<body>
		<h1>Ejemplo de operaciones de comparacion en PHP</h1>
		<?php 
			$a = 8;
			$b = 3;
			$c = 3;
			echo $a == $b, "<br>";	//Esta comparacion es Falsa
			echo $a != $b, "<br>";	//Esta comparacion es Verdadera
			echo $a < $b, "<br>";	//Esta comparacion es Falsa
			echo $a > $b, "<br>";	//Esta comparacion es Verdadera
			echo $a >= $c, "<br>";	//Esta comparacion es Verdadera
			echo $a <= $c, "<br>";	//Esta comparacion es Falsa
		 ?>
	</body>
</html>