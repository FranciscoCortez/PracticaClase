<?php 
	$un_bool = TRUE;				//valor booleano
	$un_str = "Programacion";		//una cadena
	$un_str2 = "Programacion";		//una cadena
	$un_int = 12;					//un entero

	echo gettype($un_bool); 			//Imprime:booleano
	echo " ";
	echo gettype($un_str);			//Imprime:String

	//Si este valor es un entero, incrementarlo en cuatro
	if (is_int($un_int)) {
		$un_int +=4; 
	}

	//si $un_bool es una cadena, imprimirla
	// (no imprime nada)
	if (is_string($un_bool)) {
		echo "Cadena: $un_bool";
	}
 ?>