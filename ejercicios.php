<?php
// variables para el ejercicio
$i = 9;
$f = 33.5;
$c = 'x';

echo ($i >= 6) && ($c == ‘X’), "<br>";         //El resultado es false
echo ($i >= 6) || ($c == 12), "<br>";		   //El resultado es true
echo ($f < 11) && ($i > 100), "<br>";          //El resultado es false
echo ($c != ‘P’) || (($i + $f) <= 10), "<br>"; //El resultado es true
echo $i + $f <= 10, "<br>";					   //El resultado es false
echo $i >= 6 && $c == ‘X’, "<br>";             //El resultado es false
echo $c != ‘P’ || $i + $f <= 10, "<br>";       //El resultado es true
?>